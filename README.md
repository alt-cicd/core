<img src="docs/images/alt-logo.png" alt="alt-cicd" width="300"/>

<a name="heading">alt-cicd module: core</a>
==============================================

<a name="titlebar">Introduction</a>
---------------------------

The alt-cicd **core** module provides the essential pipeline components required and extended by all other
alt-cicd library modules.

The key benefit of a shared core is in having common set of foundational blocks for development teams, that creates both
a shared language, and an ecosystem of consistent patterns across a developer cohort, without being "too" proscriptive
about how teams should work, or limiting their project pipeline design choices.

## <a name="contents">Contents</a>
1. [Variables](#variables)
2. [Stages](#stages)
3. [Workflow](#workflow)
4. [Templates](#templates)
    1. [Variables](#template-variables)
    2. [Rules](#template-rules)
    3. [Scripts](#template-scripts)
    1. [Jobs](#template-jobs)
5. [Jobs](#jobs)
    1. [dotenv](#dotenv)
    1. [noop](#noop)
    1. [src_sha256sum](#src_sha256sum)
    1. [pages](#pages)

<a name="variables">Variables</a>
========================================

The alt-cicd modules adopt the `CICD_` prefix, to:
* indicate that they are used through-out the alt-cicd module pipelines.
* separate them from other GitLab `CI_` variables.

Variables are used as:
* constants,
* configuration (patterns)
* switches/toggles

Where a variable is used as a behaviour toggle, it is **off** by default, making all behaviours in the alt-cicd
modules 'opt-in'.  The benefit of opting in, is that there are no "magic" behaviours, and the developer is required
both understand and explicitly define what is included in the build.

The code module includes two Web UI form variables, `CICD_JOB` and `CICD_GOAL` used by jobs that support them to
include and exclude jobs from the pipeline being triggered.

All alt-cicd variables are documented in-line.  The current set of core global variables is:

```yaml
variables:
   CICD_DOTENV_ENABLED: 'false'                                                            # The dotenv job in .pre stage does not run if not set to 'true'.  The dotenv job is fundamental to the library design.
   CICD_DOTENV_FILEPATH: '.env'                                                            # Default location of the key-value pairs file created by dotenv and loaded by the .default job script template
   CICD_DOTENV_PRINT: 'false'                                                              # Print the .env file list before on loading
   CICD_DOTENV_SCOPE: 'pipeline'                                                           # .default template script loads vars from pipeline artefact file, otherwise re-creates vars and file in the job (i.e. if set to 'job')
   CICD_FAIL_DEPRECATED_JOBS_ENABLED: 'true'                                               # Flag can be used by deprecated jobs to fail and warn, see .default_deprecated
   CICD_GOAL_DEFAULT: 'default'                                                            # The text of the default goal, (if changing must search for relevant patterns that use it).
   CICD_GOAL_RULE_TEMPLATES_ENABLED: 'false'                                               # Goal related jobs are included according to goal based rule templates, if enabled (opt-in)
   CICD_GOAL_WORKFLOW_TEMPLATES_ENABLED: 'false'                                           # Goal related workflow variables are set according to goal based rule templates, if enabled (opt-in)
   CICD_MODULE_CORE: 'true'                                                                # Identifies that the core module is included / active.
   CICD_MODULE_CORE_VERSION: '2.5.0'                                                       # Identifies the core module version
   CICD_NEVER_ON_MERGE_REQUEST_ENABLED: 'false'                                            # Exclude jobs matching CICD_NEVER_ON_MERGE_REQUEST_PATTERN on merge pipelines
   CICD_NEVER_ON_MERGE_REQUEST_PATTERN: '/.*/'                                             # Exclude jobs matching on merge pipelines, if enabled
   CICD_NOOP_ENABLED: 'true'                                                               # The noop build job in build stage does not run if not set to 'true'. Required for otherwise empty pipelines
   CICD_PAGES_ENABLED: 'false'                                                             # GitLab pages publishing is opt-in, off by default
   CICD_PAGES_GOAL_PATTERN: '/^(default)^/'                                                # Goal(s) used publish to pages
   CICD_PAGES_PATH: 'public'                                                               # Gitlab pages site path
   CICD_PROJECT_NAME: '$CI_PROJECT_NAME'                                                   # Can be used to statically manage the project name, default to Gitlab project name.
   CICD_PROJECT_VERSION: '0.0.0'                                                           # Can be used to statically manage the project version, though is more generally over-ridden dynamically by the dotenv job.
   CICD_PROJECT_VERSION_CHANGESET: 'changeset'                                             # The changeset name applied to prereleases, where CICD_VERSION_METADATA is set as 'changeset' e.g. if set as working the version would be 1.0.0-working
   CICD_PROJECT_VERSION_CHANGESET_SEPARATOR: '-'                                           # Used as the standard semantic version format unstable release suffix separator e.g 1.0.0-Q1, can be changed if deviating from sem ver.
   CICD_PROJECT_VERSION_METADATA_ENABLED: 'false'                                          # Appends semver 2.0 build metadata to final-release CICD_PROJECT_VERSION if enabled, e.g: 1.0.0+<build>.<timestamp>.<commit_sha>
   CICD_PROJECT_VERSION_METADATA_SEPARATOR: '+'                                            # Used as the standard semantic version format unstable release build metadata suffix separator e.g. 1.0.0-Q1+joed, can be changed if deviating from sem ver.
   CICD_SRC_SHA256SUM_ENABLED: 'false'                                                     # The src_sha256sum job will not run if not to to 'true'
   CICD_SRC_SHA256SUM_FILEPATH: 'src.idempotent.tar.gz'                                    # The file location of the idempotent GNU tar archive used to generate the source sha256sum
   CICD_WORKFLOW_COMMIT_BRANCH_NOBUILD_PATTERN: /^scratch\//                               # Excludes pipeline builds when CI_COMMIT_BRANCH matches, allowing scratch and experimental branches if enabled
   CICD_WORKFLOW_COMMIT_BRANCH_NOBUILD_PATTERN_ENABLED: 'false'                            # Enables no build branch pattern checking (see above)
   CICD_WORKFLOW_COMMIT_TAG_NOBUILD_ON_ENV_PATTERN: /^(dev|tst|prd|latest)$/               # Excludes pipeline builds on push of environment tags (e.g. dev tag represents commit_sha deployed to dev)
   CICD_WORKFLOW_COMMIT_TAG_NOBUILD_ON_ENV_PATTERN_ENABLED: 'false'                        # Excludes pipeline builds on push of environment tags (e.g. dev tag represents commit_sha deployed to dev)
   CICD_WORKFLOW_COMMIT_TAG_NOBUILD_ON_VERSION_PATTERN: /^v?\d\.\d\.\d[\+-]?.*/            # Excludes pipeline builds on push of pre-release semantic version tags
   CICD_WORKFLOW_COMMIT_TAG_NOBUILD_ON_VERSION_PATTERN_ENABLED: 'false'                    # Enables no build on push of pre-release semantic version tagschecking (see above)
   CICD_WORKFLOW_COMMIT_TITLE_NOBUILD_PATTERN: /^scratch:/                                 # Excludes pipeline builds when CI_COMMIT_TITLE matches, allowing scratch and experimental commits if enabled
   CICD_WORKFLOW_COMMIT_TITLE_NOBUILD_PATTERN_ENABLED: 'false'                             # Enables no build commit title (first line) checking (see above)
   CICD_WORKFLOW_BUILD_ON_FINAL_RELEASE_TAG_PATTERN: /^v?\d\.\d\.\d.$/                     # Pattern to identify when final semantic version tag is pushed
   CICD_WORKFLOW_BUILD_ON_FINAL_RELEASE_TAG_PATTERN_ENABLED: 'false'                       # Enables build pipeline when final semantic version tag is pushed
   CICD_WORKFLOW_SERVER_URL_NOBUILD_PATTERN: '/gitlab\.com/'                               # Disable alt-cicd library build on gitlab.com
   CICD_WORKFLOW_SERVER_URL_NOBUILD_PATTERN_ENABLED: 'false'                               # Enables no build when CI_SERVER_URL matches a patter
   CICD_RUNNER_TAG_DEFAULT: 'docker'                                                       # Used by .default job template to allocate a docker enabled runner
   CICD_RUNNER_TAG_DEFAULT_WIN: 'windows'                                                  # Used by .default_win job template to allocate a windows shell runner
   CICD_RUNTIME_ALPINE: 'alpine:1.0.0'                                                     # The default alpine image used in the library
   CICD_RUNTIME_DEBIAN_SLIM: 'debian:12.2-slim'                                            # The default debian:slim image used in the library

   #----------------------------------------------------------
   # Parameter variables for Web UI and Schedule (or API etc)
   #----------------------------------------------------------

   CICD_JOB:
      description: "Specify a pipeline job to run by name (for jobs supporting the CICD_JOB variable), eg: noop [optional]"
      value: ''
   CICD_GOAL:
      description: "Specify a pipeline goal to run (for jobs supporting the CICD_GOAL variable) [optional]"
      value: 'default'
      options:
         - 'default'
```

<a name="stages">Stages</a>
===============================

The core module defines the following common (generic), custom pipeline stages, as well as the in-built
`.pre` & `.post` stages.

The core module targets more stages (over fewer), as stages implicitly make the division of jobs and their dependencies
easier.  If no jobs occur in a stage, the stage is ommitted (invisible) to the project, so having
more (over fewer) has very little impact.

The core module uses generic stage names, and avoids specific configuration or gated environment stages such
as, for example `dev` | `test` | `prod`, because the alt-cicd modules use rules based on the `CICD_GOAL` and other
parameter variables to more flexibly determine when to run, and in what order.

The benefit of using generic stage names, such as "deploy" is a more consistent pipeline architecture that encourages a
re-usable triggering and configuration of jobs for specific goals, when used appropriately.

This core module retains all GitLab's default stages, rather the excluding them, and more may be added as the
alt-cicd modules mature.

All alt-cicd stages are documented in-line.

```yaml
#  Legend
#  -----
#  * CICD      - Added by the alt-cicd Library
#  * GitLab    - Default out-of-the-box GitLab stages

stages:
  - dependencies       # CICD      - Downloading and caching of upstream web content: npm ci, mvn dependency, curl etc
  - runtime            # CICD      - Construction of runtime containers, used for  deployment or build: dockerfile
  - static-analysis    # CICD      - Static source code analysis, linting, compliance tests: npm lint, mvn pmd, secret detection, licence scanning
  - test               # GitLab    - Dynamic testing, unit testing and code coverage of the project artefacts
  - publish            # CICD      - Pushing, uploading or placing the project artefact for downstream consumption
  - release            # CICD      - Producing and attaching (publishing) release documentation, and *tagging* the source code with the semantic release: tag_release, tag_prerelease_with_author
  - preflight          # CICD      - Checks to be run _before_ the project artefact is deployed in an environment
  - setup              # CICD      - Creating (or validating the existence of) any resources required for the deployment
  - deploy             # GitLab    - Installation and startup of the project artefact in a target environment
  - postflight         # CICD      - Steps _after_ the project artefact is deployed into the target environment, performance testing, regression testing
  - teardown           # CICD      - Complete removal of a project environment, effectively reversing both the setup and deploy
  - operate            # CICD      - Ad hoc jobs used to maintain, or operate the project in a target environment
```

<a name="workflow">Workflow</a>
======================================

Workflow rules dictate what conditions pipelines will trigger on, and over-ride job based rules (where they exist).

The alt-cicd core module allows the following ways of excluding pipelines, on:

* GitLab Server URL (e.g. nonprod and prod)
* Commit Branch
* Commit Title
* Tag (environment and semantic version)

All workflow exclusion rules are **opt-in** and the developer must set each relevant toggle variable to `true` to enable
the rule.

```yaml
workflow:
   rules:
      - if: $CI_SERVER_URL =~ $CICD_WORKFLOW_SERVER_URL_NOBUILD_PATTERN && $CICD_WORKFLOW_SERVER_URL_NOBUILD_PATTERN_ENABLED == "true"
        when: never
      - if: $CI_COMMIT_BRANCH =~ $CICD_WORKFLOW_COMMIT_BRANCH_NOBUILD_PATTERN && $CICD_WORKFLOW_COMMIT_BRANCH_NOBUILD_PATTERN_ENABLED == "true"
        when: never
      - if: $CI_COMMIT_TITLE =~ $CICD_WORKFLOW_COMMIT_TITLE_NOBUILD_PATTERN && $CICD_WORKFLOW_COMMIT_TITLE_NOBUILD_PATTERN_ENABLED == "true"
        when: never
      - if: $CI_COMMIT_TAG =~ $CICD_WORKFLOW_COMMIT_TAG_NOBUILD_ON_ENV_PATTERN && $CICD_GOAL == $CICD_GOAL_DEFAULT && $CICD_WORKFLOW_COMMIT_TAG_NOBUILD_ON_ENV_PATTERN_ENABLED == "true"
        when: never
      - if: $CI_COMMIT_TAG =~ $CICD_WORKFLOW_BUILD_ON_FINAL_RELEASE_TAG_PATTERN && $CICD_GOAL == $CICD_GOAL_DEFAULT && $CICD_WORKFLOW_BUILD_ON_FINAL_RELEASE_TAG_PATTERN_ENABLED == "true"
        when: always
      - if: $CI_COMMIT_TAG =~ $CICD_WORKFLOW_COMMIT_TAG_NOBUILD_ON_VERSION_PATTERN && $CICD_GOAL == $CICD_GOAL_DEFAULT && $CICD_WORKFLOW_COMMIT_TAG_NOBUILD_ON_VERSION_PATTERN_ENABLED == "true"
        when: never
      - !reference [.rules_goals_workflow_1, rules]
      - !reference [.rules_goals_workflow_2, rules]
      - if: $CI_COMMIT_BRANCH
        when: always
      - if: $CI_COMMIT_TAG
        when: always
      - if: $CI_COMMIT_REF_NAME


```
<a name="templates">Templates</a>
======================================

## <a name="template-variables">Variables</a>

Each module contains a matching variable template, including the name of the module, which jobs in the module
extend:

```yaml
#----------------------------------------------------------
# Templates - Variables
#----------------------------------------------------------

.core:                                          # Module specific variable over-rides, referenced in module jobs
   variables:
      CICD_MODULE_NAME: 'core'
      CICD_MODULE_VERSION: $CICD_MODULE_CORE_VERSION
```
## <a name="template-rules">Rules</a>

The core module includes template rules that are referenced by jobs throughout the library.

The `.rules_never_on_cicd_job` is used widely to prevent jobs running when the job indicated by the `CICD_JOB` environment
variable is set.

```yaml

#----------------------------------------------------------
# Templates - Rules
#----------------------------------------------------------

.rules_goals_workflow_1 :                       # Over-ride to set pipeline goal parameter variables on custom conditions
   rules:
      - if: $CICD_GOAL == $CICD_GOAL_DEFAULT && $CICD_GOAL_WORKFLOW_TEMPLATES_ENABLED == "true" && "true" == "false"
        variables:
           CICD_GOAL: $CICD_GOAL_DEFAULT

.rules_goals_workflow_2 :                       # Over-ride to set pipeline goal parameter variables on custom conditions
   rules:
      - if: $CICD_GOAL == $CICD_GOAL_DEFAULT && $CICD_GOAL_WORKFLOW_TEMPLATES_ENABLED == "true" && "true" == "false"
        variables:
           CICD_GOAL: $CICD_GOAL_DEFAULT
           
.rules_never_on_cicd_job:                        # Used to prevent jobs running when another job is indicated by the `CICD_JOB` variable
  rules:
    - if: $CICD_JOB
      when: never
```

It is always referenced following a primary rule, as follows:

```yaml
  rules:
    - if: $CICD_JOB == $CI_JOB_NAME
    - !reference [.rules_never_on_cicd_job, rules]
```

> The primary rule cannot be referenced, as the `$CI_JOB_NAME` variable resolves to the template name in such cases (
> perhaps not the expected behaviour).

## <a name="template-scripts">Scripts</a>

The core module defines:

* the `.default_dotenv_load:` that preloads calculated variables from the .env file
* the global unix-flavoured `.default_header:`, that leads all module jobs:
* The `.dotenv_core:` minimal set of calculated variables used by the modules

The are set is scripts to allow for over-riding and recomposition by other modules
and jobs in the library.
```yaml
#----------------------------------------------------------
# Templates - Scripts
#----------------------------------------------------------

.default_deprecated:
   script:
      - |
         # fail if deprecated
         if test "$CICD_JOB_DEPRECATED" != "" ; then
           echo "===================================================================="
           echo "CICD $CI_JOB_NAME is DEPRECATED and may be removed in a future."
           echo "  version."
           if test "$CICD_FAIL_DEPRECATED_JOBS_ENABLED" != "false" ; then
              echo ""
              echo "Set CICD_FAIL_DEPRECATED_JOBS_ENABLED to false to continue using."
              echo "===================================================================="
              exit 1
            else
              echo "===================================================================="
           fi;
         fi;

.default_dotenv_load:
   script:
      - |
         # load pipeline $CICD_DOTENV_FILEPATH file variables
         #   if $CICD_DOTENV_FILEPATH exists && is non-empty
         if test -s $CICD_DOTENV_FILEPATH ; then
             echo "$CICD_DOTENV_FILEPATH file found, exporting variables."
             export $(cat $CICD_DOTENV_FILEPATH | xargs)
         fi
         if test "$CICD_DOTENV_PRINT" == 'true' ; then
             echo "$CICD_DOTENV_FILEPATH variable set is:"
             cat $CICD_DOTENV_FILEPATH
         fi

.default_dotenv_load_win:
   script:
      - |
         # load pipeline $CICD_DOTENV_FILEPATH file variables
         #   if $CICD_DOTENV_FILEPATH exists && is non-empty
         Get-Content $env:CICD_DOTENV_FILEPATH | foreach {
           $name, $value = $_.split('=')
           Set-Content env:\$name $value
         }
         if ("$env:CICD_DOTENV_PRINT" -eq $True) {
           echo "$env:CICD_DOTENV_FILEPATH variable set is:"
           Get-Content $env:CICD_DOTENV_FILEPATH
         }

.default_header:
   script:
      - |
         #
         echo "===================================================================="
         echo "alt-cicd/core module version:" $CICD_MODULE_CORE_VERSION
         echo "--------------------------------------------------------------------";
         echo "";
         echo "Project    : $CICD_PROJECT_NAME";
         echo "Branch/Tag : $CI_COMMIT_REF_NAME";
         echo "Module     : $CICD_MODULE_NAME $CICD_MODULE_VERSION";
         echo "Stage      : $CI_JOB_STAGE";
         echo "Job        : $CI_JOB_NAME - id ($CI_JOB_ID) - started at $CI_JOB_STARTED_AT";
         echo "";
         echo "Job Detail : $CI_JOB_URL";
         echo "";
         if test "$CICD_JOB_DOC_URL" != "" ; then
           echo "";
           echo "Job Documentation: $CICD_JOB_DOC_URL";
         fi;
         echo "";
         echo "---------------------------";
         echo "CICD Compliance Framework:";
         echo "---------------------------";
         if test "$CICD_COMPLIANCE_FRAMEWORK_VERSION" != "" ; then
           echo "CICD_COMPLIANCE_FRAMEWORK_VERSION (applied): $CICD_COMPLIANCE_FRAMEWORK_VERSION";
           echo "CICD_COMPLIANCE_POLICY (applied): $CICD_COMPLIANCE_POLICY";
         else
           echo "CICD_COMPLIANCE_FRAMEWORK_VERSION (applied): none!"
         fi;
         echo "---------------------------";
         echo "CICD Variables:";
         echo "---------------------------";
         echo "CICD_GOAL: $CICD_GOAL";
         if test "$CICD_DOTENV_ENABLED" == "true" ; then
           if test -f "$CICD_DOTENV_FILEPATH" ; then
               echo "CICD_DOTENV_FILEPATH: $CICD_DOTENV_FILEPATH found";
           else
               echo "CICD_DOTENV_FILEPATH: $CICD_DOTENV_FILEPATH not found";
           fi;
         fi;
         if test "CICD_MODULE_GOALS_RELEASE" == "true"; then
           echo "--------------------------";
           echo "Goal - Release Variables:";
           echo "--------------------------";
           if test -f "$CICD_DOTENV_FILEPATH"; then
             if test "$CICD_PROJECT_VERSION_PROVIDED" == "$CICD_PROJECT_VERSION" ; then
               echo "CICD_PROJECT_VERSION: $CICD_PROJECT_VERSION_PROVIDED from $CI_CONFIG_PATH";
             else
               echo "CICD_PROJECT_VERSION: $CICD_PROJECT_VERSION from $CICD_DOTENV_FILEPATH";
             fi;
           else
               echo "CICD_PROJECT_VERSION: $CICD_PROJECT_VERSION from $CI_CONFIG_PATH";
           fi;
           echo "CICD_PROJECT_VERSION_CHANGESET: $CICD_PROJECT_VERSION_CHANGESET";
         fi;
         if test "$CICD_MODULE_GOALS_DEPLOYMENT" == "true"; then
           echo "---------------------------";
           echo "Goal - Deployment Variables:";
           echo "---------------------------";
           echo "CICD_ENV: $CICD_ENV";
           if test "$CICD_ENV_METADATA" != "" ; then
               echo "CICD_ENV_METADATA: $CICD_ENV_METADATA";
           else
               echo "CICD_ENV_METADATA: (not provided)";
           fi;
           if test "$CICD_VERSION" != "" ; then
             echo "CICD_VERSION (provided): $CICD_VERSION";
           fi;
           echo "CICD_VERSION_METADATA: $CICD_VERSION_METADATA";
         fi;
         echo "";
         echo "---------------------------";
         echo "Active Linux Distro:";
         echo "---------------------------";
         cat /etc/issue; cat /etc/os-release
         echo "---------------------------";
         echo "Active Shell:";
         echo "----------------------------"
         echo $0
         echo "===================================================================="

.default_header_win:
   script:
      - |
         # script header
         echo "===================================================================="
         echo "alt-cicd/core module version:" $env:CICD_MODULE_CORE_VERSION
         echo "--------------------------------------------------------------------"
         echo ""
         echo "Stage: $env:CI_JOB_STAGE"
         echo "Job: $env:CI_JOB_NAME - id ($env:CI_JOB_ID) - started at $env:CI_JOB_STARTED_AT"
         echo ""
         echo "Job Detail: $env:CI_JOB_URL"
         echo ""
         if ("$env:CICD_JOB_DOC_URL" -ne "") {
           echo ""
           echo "Job Documentation: $env:CICD_JOB_DOC_URL"
         }
         echo ""
         echo "-------------------------"
         echo "CICD Parameter Variables:"
         echo "-------------------------"
         if ("$env:CICD_PROJECT_VERSION" -ne "0.0.0") {
           echo "CICD_PROJECT_VERSION (provided): $env:CICD_PROJECT_VERSION"
         }
         else {
           echo "CICD_PROJECT_VERSION (derived): $env:CICD_PROJECT_VERSION"
         }
         echo "CICD_PROJECT_VERSION_CHANGESET: $env:CICD_PROJECT_VERSION_CHANGESET"
         echo "CICD_ENV: $env:CICD_ENV"
         if ("$env:CICD_ENV_METADATA" -ne "") {
             echo "CICD_ENV_METADATA: $env:CICD_ENV_METADATA"
         }
         else {
             echo "CICD_ENV_METADATA: (not provided)"
         }
         echo "CICD_GOAL: $env:CICD_GOAL"
         if ("$env:CICD_VERSION" -ne "") {
           echo "CICD_VERSION (provided): $env:CICD_VERSION"
         }
         echo "CICD_VERSION_METADATA: $env:CICD_VERSION_METADATA"
         if ("CICD_COMPLIANCE_FRAMEWORK_VERSION" -ne "") {
           echo "CICD_COMPLIANCE_FRAMEWORK_VERSION (applied): $env:CICD_COMPLIANCE_FRAMEWORK_VERSION"
           echo "CICD_COMPLIANCE_POLICY (applied): $env:CICD_COMPLIANCE_POLICY"
         }
         echo ""
         echo "-------------------------"
         Get-ComputerInfo | select WindowsProductName, WindowsVersion
         echo "-------------------------"
         Get-Host | select Version
         echo "-------------------------"
         echo "===================================================================="

.dotenv_core:
   script:
      - |
         # .dotenv_core: create core .env ($CICD_DOTENV_FILEPATH) file (to be loaded by each stage job). Set CICD_DOTENV_ENABLED to false to disable."
         echo -n >> $CICD_DOTENV_FILEPATH
         if test -f "$CICD_SRC_SHA256SUM_FILEPATH" ; then
             export CICD_SRC_SHA256SUM=`sha256sum $CICD_SRC_SHA256SUM_FILEPATH | cut -d " " -f1`;
             echo "set CICD_SRC_SHA256SUM as $CICD_SRC_SHA256SUM"
             echo CICD_SRC_SHA256SUM=$CICD_SRC_SHA256SUM >> $CICD_DOTENV_FILEPATH
             export CICD_SRC_SHA256SUM_SHORT=`echo -n $CICD_SRC_SHA256SUM | cut -c -8`
             echo "set CICD_SRC_SHA256SUM_SHORT as $CICD_SRC_SHA256SUM_SHORT"
             echo CICD_SRC_SHA256SUM_SHORT=$CICD_SRC_SHA256SUM_SHORT >> $CICD_DOTENV_FILEPATH
         fi;
         if test "$CICD_PIPELINE_CREATED_AT" == ""; then
           export CICD_PIPELINE_CREATED_AT=$CI_PIPELINE_CREATED_AT
         fi;
         export CICD_COMMIT_AUTHOR_NAME="`echo -n $CI_COMMIT_AUTHOR | cut -d "<" -f1 | xargs`"
         export CICD_COMMIT_AUTHOR_NAME_SLUG=`echo -n $CICD_COMMIT_AUTHOR_NAME | tr -cd "[:alnum:]" | tr [:upper:] [:lower:]`
         export CICD_COMMIT_AUTHOR_EMAIL=`echo -n $CI_COMMIT_AUTHOR | cut -d "<" -f2 | cut -d ">" -f1`
         export CICD_PIPELINE_CREATED_AT_TSTAMP=`echo -n $CICD_PIPELINE_CREATED_AT | sed s/-//g | sed s/T//g | sed s/Z//g | sed s/://g` >> $CICD_DOTENV_FILEPATH
         export CICD_PIPELINE_CREATED_AT_DATE=`echo -n $CICD_PIPELINE_CREATED_AT_TSTAMP | cut -c -8`
         export CICD_PIPELINE_CREATED_AT_TIME=`echo -n $CICD_PIPELINE_CREATED_AT_TSTAMP | cut -c 9-`

         echo "set CICD_COMMIT_AUTHOR_NAME as $CICD_COMMIT_AUTHOR_NAME"
         echo "set CICD_COMMIT_AUTHOR_NAME_SLUG as $CICD_COMMIT_AUTHOR_NAME_SLUG"
         echo "set CICD_COMMIT_AUTHOR_EMAIL as $CICD_COMMIT_AUTHOR_EMAIL"
         echo "set CICD_PIPELINE_CREATED_AT_DATE as $CICD_PIPELINE_CREATED_AT_DATE"
         echo "set CICD_PIPELINE_CREATED_AT_TIME as $CICD_PIPELINE_CREATED_AT_TIME"
         echo "set CICD_PIPELINE_CREATED_AT_TSTAMP as $CICD_PIPELINE_CREATED_AT_TSTAMP"

         echo CICD_COMMIT_AUTHOR_NAME_SLUG=$CICD_COMMIT_AUTHOR_NAME_SLUG >> $CICD_DOTENV_FILEPATH
         echo CICD_COMMIT_AUTHOR_EMAIL=$CICD_COMMIT_AUTHOR_EMAIL >> $CICD_DOTENV_FILEPATH
         echo CICD_PIPELINE_CREATED_AT_DATE=$CICD_PIPELINE_CREATED_AT_DATE >> $CICD_DOTENV_FILEPATH
         echo CICD_PIPELINE_CREATED_AT_TIME=$CICD_PIPELINE_CREATED_AT_TIME >> $CICD_DOTENV_FILEPATH
         echo CICD_PIPELINE_CREATED_AT_TSTAMP=$CICD_PIPELINE_CREATED_AT_TSTAMP >> $CICD_DOTENV_FILEPATH
```
## <a name="template-jobs">Jobs</a>

The core module includes template jobs referenced and extended as the basis for jobs throughout the library, including:
     
* `.cicd_alpine:`
* `.cicd_debian_slim:`
* `.default:`
* `.trigger:`

All templates are documented inline:

```yaml
#----------------------------------------------------------
# Templates - Jobs
#----------------------------------------------------------

.cicd_alpine:                                     # convenience template for alt-cicd module alpine-linux template job
   extends:
      - .default
      - .core

.cicd_debian_slim:                                # convenience template for alt-cicd module debian:slim template job, has some function alpine cannot support (e.g. gnu tar)
   extends:
      - .default
      - .core
   image: $CICD_RUNTIME_DEBIAN_SLIM

.default:                                         # the explicit default template job, based on GitLab docker runner using alpine-linux
   image: $CICD_RUNTIME_ALPINE
   script:
      - if test $CICD_DOTENV_SCOPE == 'pipeline'; then
      - !reference [.default_dotenv_load, script]
      - else
      - !reference [dotenv, before_script]
      - !reference [dotenv, script]
      - fi;
      - !reference [.default_header, script]
   tags:
      - $CICD_RUNNER_TAG_DEFAULT

.default_win:
   script:
      - !reference [.default_dotenv_load_win, script]
      - !reference [.default_header_win, script]
   tags:
      - $CICD_RUNNER_TAG_DEFAULT_WIN

.trigger:                                         # the parent trigger template for invoking child pipelines on the same project, ref and file. Used with CICD_GOAL.
   trigger:
      include:
         - project: $CI_PROJECT_PATH
           ref: $CI_COMMIT_REF_NAME
           file: $CI_CONFIG_PATH
      strategy: depend
      forward:
         pipeline_variables: false
         yaml_variables: true
```

<a name="jobs">Jobs</a>
======================================

The core module includes the following jobs job, **off** by default (except noop, to ensure a pipeline by default).

* `dotenv:`
* `noop:`
* `src_sha256sum:`
* `pages:`

## <a name="dotenv">dotenv</a>

The dotenv calculates environment variables that must be derived (or calculated) from the contents of the repository,
or other predefined variables.

It works by creating a default `.env` file (the name is configurable, see [variables](#variables)), which is
then stored as a build artifact; the build artefact is then available in subsequent stage jobs,
and can then be used by the `.default_dotenv_load:` template  `script:` section to load the file key=value pairs as
environment variables local only within the job script _(i.e. cannot be used in rules)_.

```yaml
dotenv:                                         # calculates environment variables that must be derived (or calculated) from the contents of the repository or pipeline variables, and writes to a default .env artifact file
   extends:
      - .default
      - .core
   stage: .pre
   needs:
      - job: src_sha256sum
        artifacts: true
        optional: true
   variables:
      CICD_JOB_DOC_URL: $CI_SERVER_URL/alt-cicd/core/-/tree/main/README.md#dotenv
   rules:
      - if: $CI_PIPELINE_SOURCE && $CICD_DOTENV_ENABLED == 'true'
        when: always
   before_script:
      - !reference [.default_header, script]
      - !reference [.dotenv_core, script]
   script:
      - |
         # override dotenv job script: tag to append .env ($CICD_DOTENV_FILEPATH) file, and load custom variables between jobs
   artifacts:
      paths:
         - $CICD_DOTENV_FILEPATH
```

## <a name="noop">noop</a>

The noop job, is a simple no-operation that ensures a pipeline will run when no other jobs resolve to true, useful when
initialising an alt-cicd project.

Set CICD_NOOP_ENABLED: 'true' to enable.

```yaml
noop:                                         # no-operation job, ensures a pipeline will run when no other jobs resolve to true. Set CICD_NOOP_ENABLED: 'true' to enable
  extends:
    - .default
    - .core
  stage: build
  variables:
    CICD_JOB_DOC_URL: $CICD_LIBRARY_DOCS_URL/docs/jobs/04-build/noop.md
  rules:
    - if: $CICD_JOB == $CI_JOB_NAME
    - !reference [.rules_never_on_cicd_job, rules]
    - if: $CICD_NEVER_ON_MERGE_REQUEST_ENABLED == 'true' && $CI_JOB_NAME =~ $CICD_NEVER_ON_MERGE_REQUEST_PATTERN && $CI_PIPELINE_SOURCE == 'merge_request_event'
      when: never
    - if:  $CICD_NOOP_ENABLED == 'true'
  script:
    - !reference [.default, script]
    - echo "Default no operation job (will ensure a pipeline will run, if no other jobs are included). Set CICD_NOOP_ENABLED to false to disable"

```

## <a name="src_sha256sum">src_sha256sum</a>

The sha_src job works with the [dotenv](#dotenv) job, first producing an indempotent tar.gz file,
that will produce the same sha256sum output for binary identical committed source, regardless of git commit sha/hash.  

This is useful in reverting code, or demonstrating builds are source-identical.

The [dotenv](#dotenv.md) job subsequently calculates the sha256sum, and exposes the
`CICD_PROJECT_SRC_SHA256_SUM` variable for use in publishing and release management.

See:

* [stackoverflow.com: how-to-create-a-tar-file-that-omits-timestamps-for-its-contents](https://stackoverflow.com/questions/32997526/how-to-create-a-tar-file-that-omits-timestamps-for-its-contents)
* [stackoverflow: tar-preserving-only-file-names-contents-and-executable-bit](https://stackoverflow.com/questions/45734702/tar-preserving-only-file-names-contents-and-executable-bit?noredirect=1#comment78427625_45734702)

```yaml
src_sha256sum:                               # creates an idempotent source tar for use in calculating a source sha256sum. Set CICD_SRC_SHA256SUM_ENABLED: 'true' to enable
   extends:
      - .default
      - .core
   stage: .pre
   image: $CICD_RUNTIME_DEBIAN_SLIM
   variables:
      CICD_JOB_DOC_URL: $CI_SERVER_URL/alt-cicd/core/-/tree/main/README.md#src_sha256sum
   rules:
      - if: $CI_PIPELINE_SOURCE && $CICD_SRC_SHA256SUM_ENABLED == 'true'
        when: always
   before_script:
      - !reference [.default_header, script]
   script:
      - |
         # create idempotent tar.gz. Set CICD_SRC_SHA256SUM_ENABLED to false to disable."
         touch $CICD_SRC_SHA256SUM_FILEPATH
         tar -c --sort=name --owner=root:0 --group=root:0 --mtime='UTC 1970-01-01' --exclude=$CICD_SRC_SHA256SUM_FILEPATH --exclude=.git . | gzip -n >> $CICD_SRC_SHA256SUM_FILEPATH
   artifacts:
      paths:
         - $CICD_SRC_SHA256SUM_FILEPATH
```


## <a name="pages">pages</a>

The pages job will publish to GitLab pages when enabled

```yaml
pages:
   extends:
      - .default
      - .core
   stage: publish
   rules:
      - if: $CICD_JOB == $CI_JOB_NAME
      - !reference [.rules_never_on_cicd_job, rules]
      - if: $CICD_NEVER_ON_MERGE_REQUEST_ENABLED == 'true' && $CI_JOB_NAME =~ $CICD_NEVER_ON_MERGE_REQUEST_PATTERN && $CI_PIPELINE_SOURCE == 'merge_request_event'
        when: never
      - if: $CICD_PAGES_ENABLED != 'true'
        when: never
      - if: $CICD_GOAL =~ $CICD_PAGES_GOAL_PATTERN
   script:
      - |
         # ...
         echo "Publishing the following files to gitlab pages at $CI_PAGES_URL"
         echo `ls -la $CICD_PAGES_PATH`
   artifacts:
      paths:
         - $CICD_PAGES_PATH
```